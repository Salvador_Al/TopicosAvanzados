import java.applet.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class UsoTriangle extends Applet
{
	Button b1;
	boolean dibujar=false;
	public void init()
	{
		setSize(800,600);
		b1=new Button("Generar 100 triangulos");
		add(b1);
		b1.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				dibujar=true;
				repaint();
			}
		});
	}
	
	public void paint(Graphics g)
	{
		if(dibujar)
		{
			int wx=getWidth();
			int wy=getHeight();
			Random r=new Random();
			int x,y,x1,y1,ancho,largo;
			for (int i = 0; i < 100; i++)
			{
				x=r.nextInt(wx);
				y=r.nextInt(wy);
				ancho=50+r.nextInt(150);
				largo=50+r.nextInt(150);
				x1=x+ancho;
				y1=y+largo;
				g.setColor(new Color(r.nextInt(255),r.nextInt(255),r.nextInt(255)));
				draw.triangleR(x, y, x1, y1, g);
			}
		}
	}
}
