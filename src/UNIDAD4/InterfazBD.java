package UNIDAD4;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class InterfazBD extends JFrame{
	JButton bcap,bcons;
	public InterfazBD()
	{
		super("Interfaz con la BD");
		setSize(800,600);
		
		JPanel arr=new JPanel();
		bcap=new JButton("Captura de los Clientes");
		bcons=new JButton("Reporte de Clientes");
		arr.add(bcap); arr.add(bcons);
		add(arr,BorderLayout.NORTH);
		
		bcap.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//cuadro Dialogo
				CuadClientes obj=new CuadClientes(
						InterfazBD.this,true);
				regClientes res=obj.Mostrar();
				if(res!=null)
				{
					ManejoBD M=new ManejoBD();
					M.verificarDriver();
					M.conectarBD();
					M.agregarClientes2(res.rfc,res.name,
							res.edad,res.soltero);
					
				}
			}
		});
		
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public static void main(String[] args) {
		new InterfazBD();
	}
}
