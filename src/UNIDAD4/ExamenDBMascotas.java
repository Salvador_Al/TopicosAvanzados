package UNIDAD4;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ExamenDBMascotas
{
	Connection conex;
	public void verificarDriver()
	{
		try{ 
			Class.forName("com.mysql.jdbc.Driver");
			 }
			catch (ClassNotFoundException e)
			{    e.printStackTrace();
			     System.exit(0); 
			 }
		System.out.println("Driver perfectamente instalado");
	}
	public void conectarBD()
	{
		try{
			conex = DriverManager.getConnection(
					"jdbc:mysql://localhost/mydb?useSSL=false","root","root");
		} catch(SQLException e){ 
			System.out.println("error con la conexion con la bd"); }
		if(conex!=null)
		{
			System.out.println(
				"conectado a la bd de "
				+ "manera exitosa");}
	}
	public void ConsCliEsp(int idcliente)
	{
		try{
			Statement S=conex.createStatement();
			ResultSet res= S.executeQuery(
					"SELECT * FROM clientes where idClientes="+idcliente);
			while(res.next())
			{
				System.out.println("id Cliente: "+res.getString(1)+
									"\nNombre: "+res.getString(2)+
									"\nEdad: "+res.getString(3));
			}
			S.close();
			res.close();
		}catch(SQLException e){}
	}
	public void ConsCliDosYears(int years)
	{
		try{
			Statement S=conex.createStatement();
			ResultSet res= S.executeQuery(
					"select clientes.Nombre\r\n" + 
					"From clientes \r\n" + 
					"where idClientes in(select propietario from mascotas where edad>="+years+")");
			while(res.next())
			{
				System.out.println("Nombre: "+res.getString(1));
			}
			S.close();
			res.close();
		}catch(SQLException e){}
	}
	public static void main(String[] args) {
		ExamenDBMascotas obj=new ExamenDBMascotas();
		obj.verificarDriver();
		obj.conectarBD();
		//Consultas de clienes especificos
		obj.ConsCliEsp(1);
		System.out.println();
		obj.ConsCliEsp(2);
		//Consulta de clientes que tienen mascotas que son mayores a 2 anos
		obj.ConsCliDosYears(2);
		obj.ConsCliDosYears(10);
	}
}
