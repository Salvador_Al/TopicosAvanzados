package UNIDAD4;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
public class ManejoBD {
	Connection conex;
	public void verificarDriver()
	{
		try{ 
			Class.forName("com.mysql.jdbc.Driver");
			 }
			catch (ClassNotFoundException e)
			{    e.printStackTrace();
			     System.exit(0); 
			 }
		System.out.println("Driver perfectamente instalado");
	}
	public void conectarBD()
	{
		try{
			conex = DriverManager.getConnection(
					"jdbc:mysql://localhost/sinsiniestro?useSSL=false","root","root");
		} catch(SQLException e){ 
			System.out.println("error con la conexion con la bd"); }
		if(conex!=null)
		{
			System.out.println(
				"conectado a la bd de "
				+ "manera exitosa");}
	}
	public void consultaClientes()
	{
		try{
			Statement S=conex.createStatement();
			ResultSet res= S.executeQuery(
					"SELECT * FROM Clientes");
			while(res.next())
			{
				System.out.println("rfc: "+res.getString(1)+
						         " nombre: "+res.getString(2)+
						         " edad: "+res.getInt(3)+
						         " Casado "+res.getBoolean(4));
			}
			S.close();
			res.close();
		}catch(SQLException e){}
	}
	public void agregarClientes(String rfc,String name,int edad,int casado)
	{
		try{
			Statement s=conex.createStatement();
			s.executeUpdate("INSERT INTO Clientes(Rfc,Nombre,Edad,Soltero) " +
			"VALUES('"+rfc+"','"+name+"','"+edad+"','"+casado+"')");
			}catch(SQLException e)
			{
				e.printStackTrace();
				System.out.println("Error de insercion");
			}
	}
	public void agregarClientes2(
			String rfc,String n,int e,int s)
	{
		String sentencia="INSERT INTO Clientes"
		+ "(Rfc,Nombre,Edad,Soltero) VALUES(?,?,?,?);";
		try{
			PreparedStatement ps=
				conex.prepareStatement(sentencia);
			ps.setString(1,rfc);
			ps.setString(2, n);
			ps.setInt(3, e);
			ps.setInt(4, s);
			if(ps.executeUpdate()==1)
				System.out.println("se agrego un registro");  else System.out.println("no se agrego nada");
		}catch(SQLException e1){
			System.out.println("Error de escritura");
			e1.printStackTrace();
		}
	}
	public static void main(String[] args) {
		ManejoBD obj=new ManejoBD();
		obj.verificarDriver();
		obj.conectarBD();
		obj.consultaClientes();
		//obj.agregarClientes2("ROME741522","Rodrigo",30,0);
	}
}
