package UNIDAD4;

import java.awt.Choice;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
class regClientes{
	String rfc,name;
	int edad,soltero;
}
public class CuadClientes extends JDialog {
    JLabel et1,et2,et3,et4;
    JTextField ct1,ct2;
    JRadioButton rb1,rb2;
    ButtonGroup grupo;
    Choice ledad;
    JButton ac,ca;
    regClientes dev=null;
    public CuadClientes(JFrame v,boolean modal)
    {
    	   super(v,modal);
    	   setSize(600,250);
    	   setTitle("Captura de datos de los Clientes");
    	   setLocationByPlatform(true);
    	   et1=new JLabel("Dame el rfc del cliente: ");
    	   et2=new JLabel("Dame el nombre del cliente:");
    	   et3=new JLabel("Selecciona la edad:");
    	   et4=new JLabel("Soltero o Casado: ");
    	   ct1=new JTextField(13);
    	   ct2=new JTextField(45);
    	   ledad=new Choice();
    	   for(int i=18;i<90;i++)
    		   ledad.add(i+"");
    	   rb1=new JRadioButton("Soltero",true);
    	   rb2=new JRadioButton("Casado");
    	   grupo=new ButtonGroup();
    	   grupo.add(rb1);grupo.add(rb2);
    	   ac=new JButton("Aceptar");
    	   ca=new JButton("Cancelar");
    	   setLayout(new GridLayout(5,2));
    	   add(et1);add(ct1); //rfc
    	   add(et2);add(ct2); //nombre
    	   add(et3);add(ledad); //edad
    	   add(et4);
    	   JPanel der=new JPanel();
    	   der.add(rb1);der.add(rb2);
    	   add(der);
    	   add(ac);add(ca);
    	   ac.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dev=new regClientes();
				dev.rfc=ct1.getText();
				dev.name=ct2.getText();
				dev.edad=18+ledad.getSelectedIndex();
				if(rb1.isSelected())
					dev.soltero=0;
				else
					dev.soltero=1;
				setVisible(false);
				dispose();
			}
		});
    	   ca.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dev=null;
				setVisible(false);
				dispose();
			}
		});
    	   setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
    }
    public regClientes Mostrar()
    {
    		setVisible(true);
    		return dev;
    }
}
