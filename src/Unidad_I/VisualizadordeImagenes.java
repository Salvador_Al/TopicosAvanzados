package Unidad_I;

import java.awt.*;
import java.applet.*;
import java.net.*;
import java.awt.event.*;


public class VisualizadordeImagenes extends Applet
{
	Image vImag[];
	String Titulos[];
	Button ba,bs;
	Label etiq;
	int numIm=0;
	public void init()
	{
		setSize(800,600);
		vImag=new Image[5];
		Titulos=new String[]{"img Random 1", "img Random 2","Mustang 1", "mustang 2", "Mustang 3"};
		for (int i = 0; i < vImag.length; i++)
		{
			URL ruta=getClass().getResource("/Unidad_I/imag/im"+(i+1)+".jpg");
			vImag[i]=getImage(ruta);
		}
		ba=new Button("Imagen Anterior");
		bs=new Button("Imagen Siguiente");
		etiq=new Label(Titulos[numIm]);
		add(ba);
		add(etiq);
		add(bs);
		bs.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				numIm++;
				if (numIm==vImag.length)
					numIm=0;
				etiq.setText(Titulos[numIm]);
				repaint();
			}
		});
		ba.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				numIm--;
				if (numIm<0)
					numIm=vImag.length-1;
				etiq.setText(Titulos[numIm]);
				repaint();
			}
		});
	}
	
	public void paint(Graphics g)
	{
		g.drawImage(vImag[numIm], 100, 100, 550, 400,this);
	}
}
