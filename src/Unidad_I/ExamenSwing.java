package Unidad_I;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.net.*;
import java.util.Collections;
import java.util.LinkedList;

public class ExamenSwing extends JFrame implements ActionListener
{
	
	/* EXAMEN DE SWING
	 * Nombre: Jesus Salvador Alcala Arroyo
	 * Num. Ctrl: 15-420-460
	 * */
	JToggleButton[] vColIzq, vColDer;
	JButton in;
	ImageIcon colores[] = new ImageIcon[6];
	JToggleButton[] tb;
	int error = 0, acertados = 0, r;
	String nomcol[] = { "Orange", "Red", "Green", "Purple", "Yellow", "Blue" };

	public ExamenSwing()
	{
		setSize(400, 600);
		setTitle("Colores En Ingles");
		setResizable(false);
		setLocation(200, 100);
		Panel derecha = new Panel(new GridLayout(6, 1));
		Panel centro = new Panel(new GridLayout(6, 1));
		Panel izquierda = new Panel(new GridLayout(6, 1));
		vColDer = new JToggleButton[6];
		vColIzq = new JToggleButton[6];
		tb = new JToggleButton[6];
		in = new JButton("Inicio");

		for (int P = 0; P < tb.length; P++)
		{
			tb[P] = new JToggleButton(nomcol[P]);
			centro.add(tb[P]);
			tb[P].setName(nomcol[P]);
			tb[P].setEnabled(false);
			tb[P].addActionListener(this);
		}

		for (int i = 0; i < colores.length; i++)
		{
			URL ruta = getClass().getResource("/Unidad_I/colores/color" + (i + 1) + ".png");
			colores[i] = new ImageIcon(ruta);
		}
		for (int i = 0; i < vColIzq.length; i++)
		{
			vColIzq[i] = new JToggleButton("");
			vColDer[i] = new JToggleButton("");

			vColIzq[i].setIcon(colores[i]);
			vColDer[i].setIcon(colores[i]);

			vColIzq[i].setName(nomcol[i]);
			vColDer[i].setName(nomcol[i]);

			izquierda.add(vColIzq[i]);
			derecha.add(vColDer[i]);
			vColIzq[i].addActionListener(this);
			vColDer[i].addActionListener(this);
		}

		add(izquierda, BorderLayout.WEST);
		add(centro, BorderLayout.CENTER);
		add(derecha, BorderLayout.EAST);
		add(in, BorderLayout.SOUTH);
		in.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				CenterAct(true);
				LinkedList<Integer> izq = new LinkedList<Integer>();
				LinkedList<Integer> der = new LinkedList<Integer>();
				for (int i = 0; i < 6; i++)
				{
					izq.add(i);
					der.add(i);
				}
				Collections.shuffle(izq);
				Collections.shuffle(der);
				for (int i = 0; i < izq.size(); i++)
				{
					vColIzq[i].setIcon(colores[izq.get(i)]);
					vColIzq[i].setName(nomcol[izq.get(i)]);
				}
				for (int j = 0; j < der.size(); j++)
				{
					vColDer[j].setIcon(colores[der.get(j)]);
					vColDer[j].setName(nomcol[der.get(j)]);
				}
				in.setEnabled(false);
			}
		});

		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void CenterAct(boolean ban)
	{
		for (int i = 0; i < tb.length; i++)
			tb[i].setEnabled(ban);
	}

	public void IzqDerAct()
	{
		for (int i = 0; i < vColIzq.length; i++)
		{
			vColIzq[i].setEnabled(true);
			vColDer[i].setEnabled(true);
		}
	}

	public void actionPerformed(ActionEvent e)
	{
		for (int i = 0; i < tb.length; i++)
			if (tb[i].isSelected())
			{
				for (int j = 0; j < vColIzq.length; j++)
					if (vColIzq[j].isSelected())
					{
						for (int k = 0; k < vColDer.length; k++)
							if (vColDer[k].isSelected())
							{
								if (vColIzq[j].getName() == tb[i].getName() && vColDer[k].getName() == tb[i].getName())
								{
									vColIzq[j].setEnabled(false);
									vColDer[k].setEnabled(false);
									tb[i].setEnabled(false);
									vColIzq[j].setSelected(false);
									vColDer[k].setSelected(false);
									tb[i].setSelected(false);
									acertados++;
									if (acertados == 6)
									{
										JOptionPane.showMessageDialog(null, "you win!");
										IzqDerAct();
										in.setEnabled(true);
										error = 0;
										acertados = 0;
									}
								}
								else
								{
									error++;
									JOptionPane.showMessageDialog(null, "Error #" + error);
									vColIzq[j].setSelected(false);
									vColDer[k].setSelected(false);
									tb[i].setSelected(false);
									if (error == 3)
									{
										JOptionPane.showMessageDialog(null, "You lost... lucky next time!");
										ReAct();
										CenterAct(false);
										in.setEnabled(true);
										error = 0;
										acertados = 0;
									}
								}

							}
						return;
					}
			}
	}
	
	public void ReAct()
	{
		for (int i = 0; i < vColIzq.length; i++)
		{
			vColIzq[i].setEnabled(true);
			vColDer[i].setEnabled(true);
			vColIzq[i].setSelected(false);
			vColDer[i].setSelected(false);
		}
	}

	public static void main(String[] args)
	{
		new ExamenSwing();
	}
}
