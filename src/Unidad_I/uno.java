package Unidad_I;

import java.awt.*;
import java.applet.*;

public class uno extends Applet
{
	String texto = "";

	public void init()
	{
		Color micolor = new Color(129, 197, 167);
		this.setSize(600, 400);
		setBackground(micolor);
		setForeground(Color.blue);
		texto = "<Init>";
	}

	public void start()
	{
		texto += "<start>";
	}

	public void paint(Graphics g)
	{
		texto += "<paint>";
		g.drawString(texto, 50, 100);
	}

	public void stop()
	{
		texto += "<stop>";
	}

	public void destroy()
	{
		System.out.println("Finalizo el applet");
	}
}
