package Unidad_I;

import java.applet.*;
import java.awt.*;
import java.awt.event.*;

class precionar extends MouseAdapter
{
	MoverFigura r;

	public precionar(MoverFigura m)
	{
		r = m;
	}

	public void mousePressed(MouseEvent e)
	{
		int cx = e.getX();
		int cy = e.getY();
		int ncx = r.px - (r.ancho / 2);
		int ncy = r.py - (r.largo / 2);
		if (cx >= ncx && cy <= (ncx + r.ancho) && (cy >= ncy && cy <= (ncy + r.largo)))
			r.mover = true;
		else
			r.mover = false;
		r.repaint();
	}
}

class arrastrar extends MouseMotionAdapter
{
	MoverFigura r;

	public arrastrar(MoverFigura m)
	{
		r = m;
	}

	public void mouseDragged(MouseEvent e)
	{
		if (r.mover) // si r es verdadero, se va a mover
		{
			int cx = e.getX();
			int cy = e.getY();
			r.px = cx;
			r.py = cy;
			r.repaint();
		}
	}
}

public class MoverFigura extends Applet implements MouseWheelListener, KeyListener
{
	int px, py, ancho, largo;
	boolean mover = false;
	int velocidad=1;

	public void init()
	{
		setSize(800, 600);
		setBackground(Color.LIGHT_GRAY);
		px = 375;
		py = 250;
		ancho = 150;
		largo = 100;
		addMouseListener(new precionar(this));
		addMouseMotionListener(new arrastrar(this));
		addMouseWheelListener(this);
		addKeyListener(this);
	}

	public void paint(Graphics g)
	{
		g.setColor(Color.BLUE);
		g.fillRoundRect(px - (ancho / 2), py - (largo / 2), ancho, largo, 45, 45);
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e)
	{
		if (e.getWheelRotation() < 0)
		{ // rueda hacia arriba -- mas grande
			ancho += e.getScrollAmount();
			largo += e.getScrollAmount();
		}
		else
		{ // rueda hacia abajo -- mas chico
			ancho -= e.getScrollAmount();
			largo -= e.getScrollAmount();
		}
		repaint();
	}

	@Override
	public void keyPressed(KeyEvent e)
	{
		int valor = e.getKeyCode();
		// System.out.println("tecla presionada "+e.getKeyText(valor)+"
		// "+valor);
		
		Dimension d=getSize();
		switch (valor) {
			case KeyEvent.VK_RIGHT:
				px+=velocidad;
				if(px>d.width)
					px=-(ancho/2);
				break;
			case KeyEvent.VK_LEFT:
				px-=velocidad;
				if(px<0)
					px=d.width;
				break;
			case KeyEvent.VK_UP:
				py-=velocidad;
				if(py<0)
					py=d.height;
				break;
			case KeyEvent.VK_DOWN:
				py+=velocidad;
				if(py>d.height)
					py-=(largo/2);
				break;
			case KeyEvent.VK_F1:
				velocidad+=3;
				if(velocidad==19)
					velocidad=1;
				break;
		}
		repaint();
	}

	@Override
	public void keyReleased(KeyEvent arg0)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent arg0)
	{
		// TODO Auto-generated method stub

	}
}
