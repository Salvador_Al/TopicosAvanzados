package Unidad_I;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.awt.*;
import java.util.*;

public class SusoTablas extends JFrame
{
	JTable tabla;
	DefaultTableModel modelo;
	JButton ba;

	public SusoTablas()
	{
		setSize(800, 600);
		Vector tit = new Vector<String>();
		tit.add("Nombre");
		tit.add("Edad");
		tit.add("Sexo");
		Vector<Vector> datos = new Vector<Vector>();
		Vector al1 = new Vector();
		al1.add("Joaquin");
		al1.add(18);
		al1.add('M');
		Vector al2 = new Vector();
		al2.add("Andrea");
		al2.add(22);
		al2.add('F');
		datos.add(al1);
		datos.add(al2);
		modelo = new DefaultTableModel(datos, tit)
		{
			public void addRow(Vector rowData) {
				Integer x=(Integer)rowData.get(1);
				if(x.intValue()<70)
					super.addRow(rowData);
			}
			
			public boolean isCellEditable(int row, int column)
			{
				if (column == 1)
					return true;
				return false;
			}
		};
		tabla = new JTable(datos, tit);
		// tabla.setModel(modelo);
		JScrollPane p = new JScrollPane(tabla);
		add(p);
		ba = new JButton("Agregar un alumno");
		ba.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				cuadroD lec=new cuadroD(SusoTablas.this,true);
				Alumnos A=lec.mostrar();
				if(A!=null)
				{
					Vector v=new Vector();
					v.add(A.getName());
					v.add(A.getEdad());
					v.add(A.getSexo());
					modelo.addRow(v);
				}
			}
		});
		add(ba,BorderLayout.SOUTH);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	

	public static void main(String[] args)
	{
		new SusoTablas();
	}
}
