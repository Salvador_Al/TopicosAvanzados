package Unidad_I;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.applet.*;

public class cuatro extends Applet implements ActionListener
{
	Button b1, b2;
	int x;

	public void init()
	{
		setSize(600, 400);
		FlowLayout flujo = new FlowLayout(FlowLayout.CENTER);
		setLayout(flujo);
		b1 = new Button("Incrementar");
		b1.addActionListener(this);
		b2 = new Button("Decrementar");
		b2.addActionListener(this);
		x = 100;
		add(b1);
		add(b2);
	}

	public void paint(Graphics g)
	{
		g.setColor(Color.blue);
		g.fillRect(50, 100, x, 100);
	}

	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == b1)
			x += 100;
		if (e.getSource() == b2)
			x -= 100;
		if (x < 0)
			x = 0;

		repaint();
	}
}
