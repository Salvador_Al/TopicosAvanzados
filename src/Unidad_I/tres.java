package Unidad_I;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.applet.*;

public class tres extends Applet implements ActionListener
{
	Button b1, b2;
	int nvp;

	public void init()
	{
		setSize(600, 400);
		FlowLayout flujo = new FlowLayout(FlowLayout.RIGHT);
		setLayout(flujo);
		b1 = new Button("Push Me");
		b1.addActionListener(this);
		b2 = new Button("Restart");
		b2.addActionListener(this);
		nvp = 0;
		add(b1);
		add(b2);
	}

	public void paint(Graphics g)
	{
		g.setColor(Color.blue);
		g.drawString("El boton se presiono " + nvp + " veces", 50, 100);
	}

	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == b1)
			nvp++;
		if (e.getSource() == b2)
			nvp = 0;

		repaint();
	}
}
