package Unidad_I;

import java.applet.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JOptionPane;

public class Ejercicio2 extends Applet implements ActionListener {
	Label et;
	TextField ct;
	Button b1, b2;

	int v[], pos;

	public void init() {
		setSize(800, 600);
		et = new Label("Dame un numero: ");
		add(et);
		ct = new TextField(5);
		add(ct);
		b1 = new Button("Insertar");
		add(b1);
		b2 = new Button("Reiniciar");
		add(b2);
		b1.addActionListener(this);
		b2.addActionListener(this);
		v = new int[10];
		pos = -1;

	}

	public void paint(Graphics g) {
		// dibujar los cuadritos
		int x = 50, y = 120, width = 75, height = 40;
		for (int i = 0; i < 10; i++) {
			g.drawRect(x, y, width, height);
			y += height;
		}
		//escribir los numeros
		x=80;y=145;
		for (int i = 0; i <= pos; i++) {
			g.drawString(v[i]+"", x, y);
			y+=height;
		}
		//Dibujar la grafica
		if(pos==9) {
			g.drawLine(200, 500, 780, 500);
			g.drawLine(250, 550, 250, 200);
			Random r =new Random();
			x=251;y=500;width=50;
			for (int i = 0; i <= pos; i++) {
				g.setColor(new Color(r.nextInt(256),r.nextInt(256),r.nextInt(256)));
				g.fillRect(x, (y-v[i]), width, v[i]);
				x+=width;
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// Insertar
		if (e.getSource() == b1) {
			String texto = ct.getText();
			try {
				int num = Integer.parseInt(texto);
				if (num >= 0 && num <= 100 && (pos+1)<10) {
					v[++pos] = num;
				}
			} catch (NumberFormatException a) {
				JOptionPane.showMessageDialog(ct, "Debe ser un numero");
			}
			ct.setText("");
			repaint();

		}
		// Reiniciar
		if (e.getSource() == b2) {
			pos=-1;
			repaint();
		}

	}

}
