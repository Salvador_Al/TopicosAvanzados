package Unidad_I;

import java.awt.*;
import java.awt.event.*;
import java.net.*;
import javax.swing.*;
import java.io.*;

public class SusoComponentes extends JFrame implements ActionListener {
	JToggleButton tb1, tb2, tb3;
	//ButtonGroup bg=new ButtonGroup();
	JPopupMenu menuE;
	JTextField ct=new JTextField(60);

	public SusoComponentes() {
		setSize(800, 600);
		add(ct,BorderLayout.SOUTH);
		setTitle("Uso JToggle, JPopUpMenu JColorChoser, JFileChoser");
		URL ruta = getClass().getResource("/Unidad_I/imag/circulo2.png");
		tb1 = new JToggleButton(new ImageIcon(ruta));
		ruta = getClass().getResource("/Unidad_I/imag/triangulo2.png");
		tb2 = new JToggleButton(new ImageIcon(ruta));
		ruta = getClass().getResource("/Unidad_I/imag/cuadrado2.png");
		tb3 = new JToggleButton(new ImageIcon(ruta));
		/*bg.add(tb1);
		bg.add(tb2);
		bg.add(tb3);*/
		JPanel arriba=new JPanel();
		arriba.add(tb1);
		arriba.add(tb2);
		arriba.add(tb3);
		add(arriba,BorderLayout.NORTH);
		tb1.addActionListener(this);
		tb2.addActionListener(this);
		tb3.addActionListener(this);
		//menu
		ruta= getClass().getResource("/Unidad_I/imag/acuarela2.png");
		menuE=new JPopupMenu();
		JMenuItem op1=new JMenuItem("Color",new ImageIcon(ruta));
		ruta= getClass().getResource("/Unidad_I/imag/ayuda2.png");
		JMenuItem op2=new JMenuItem("Ayuda",new ImageIcon(ruta));
		menuE.add(op1);
		menuE.add(op2);
		addMouseListener(new MouseAdapter() {
			public void  mousePressed(MouseEvent e)
			{
				if(e.getButton()==e.BUTTON3)
				{
					menuE.show(SusoComponentes.this,e.getX(),e.getY());
				}
			}
		});
		op1.addActionListener(new ActionListener(){ 
			public void actionPerformed(ActionEvent e) {
				JColorChooser paleta=new JColorChooser();
				Color colorAnt=color;
				color=paleta.showDialog(SusoComponentes.this,"Seleccione un color",Color.GREEN);
				if(color==null)
					color=colorAnt;
				repaint();
			}
		});
		op2.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				File arch=selArch();
				if(arch!=null)
				{
					ct.setText(arch.getAbsolutePath());
				}
			}
		});
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public File selArch()
	{
		JFileChooser sel=new JFileChooser();
		sel.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		int res=sel.showOpenDialog(this);
		if (res==JFileChooser.APPROVE_OPTION)
			return sel.getSelectedFile();
		else
			return null;  //le presiono cancelar
	}
	
	Color color=new Color(0,255,0);

	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(color);
		if(tb1.isSelected())
			g.fillOval(50,100,200,200);
		
		if(tb2.isSelected())
		{//triangulo
			Polygon triangulo=new Polygon();
			triangulo.addPoint(300,300);
			triangulo.addPoint(500,300);
			triangulo.addPoint(400,100);
			g.fillPolygon(triangulo);
		}
		if(tb3.isSelected())
		//cuadrado
			g.fillRect(550,100,200,200);

	}

	public static void main(String[] args) {
		new SusoComponentes();
	}

	public void actionPerformed(ActionEvent arg0)
	{
		repaint();
	}
}