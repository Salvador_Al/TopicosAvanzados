package Unidad_I;

import java.applet.*;
import java.awt.*;

public class Gestores extends Applet
{
	Button b[];

	public void init()
	{
		setSize(600, 400);
		FlowLayout flujo = new FlowLayout(FlowLayout.LEFT, 10, 10);
		GridLayout cuadricula = new GridLayout(4, 5, 10, 10);// rows, cols,
																// espaciado
		BorderLayout zonaGeog = new BorderLayout();
		setLayout(zonaGeog);
		b = new Button[5];
		for (int i = 0; i < b.length; i++)
		{
			b[i] = new Button("Boton " + (i + 1));

		}
		add(b[0], BorderLayout.NORTH);
		add(b[1], BorderLayout.SOUTH);
		add(b[2], BorderLayout.EAST);
		add(b[3], BorderLayout.WEST);
		add(b[4], BorderLayout.CENTER);

	}
}
