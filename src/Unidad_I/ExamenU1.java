package Unidad_I;

import java.applet.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;


/*Examen Applets
 * Nombre: Jesus Salvador Alcala Arroyo
 * Nom. Ctrl: 15420460*/
public class ExamenU1 extends Applet implements ActionListener
{
	Button b1,b2,b3,b4,b5;
	int op=0;
	Color color;
	public void init()
	{
		setSize(800,600);
		setLayout(new BorderLayout());
		b1= new Button("1");
		b2=new Button("2");
		b3=new Button("3");
		b4=new Button("4");
		b5=new Button("");
		Panel centro = new Panel();
		centro.setLayout(new GridLayout(2,2));
		centro.add(b1);
		centro.add(b2);
		centro.add(b3);
		centro.add(b4);
		add(centro,BorderLayout.CENTER);
		add(b5, BorderLayout.SOUTH);
		
		b1.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				b5.setLabel("*");
				repaint();
			}
		});
		b2.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				b5.setLabel("**");
				repaint();
			}
		});
		b3.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				b5.setLabel("***");
				repaint();
			}
		});
		b4.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				b5.setLabel("****");
				repaint();
			}
		});
		b5.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				b5.setLabel("");
				int r, g, b;
				Random R = new Random();
				r = R.nextInt(256);
				g = R.nextInt(256);
				b = R.nextInt(256);
				color = new Color(r, g, b);
				System.out.println(color);
				
				b1.setForeground(color);
				b2.setForeground(color);
				b3.setForeground(color);
				b4.setForeground(color);
				repaint();
			}
		});
	}
	
	
	public void paint(Graphics g)
	{
	}
	
	public void actionPerformed(ActionEvent e)
	{
	}
}
