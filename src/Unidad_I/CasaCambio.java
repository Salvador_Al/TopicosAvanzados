package Unidad_I;

import java.awt.*;
import java.awt.event.*;
import java.net.*;
import javax.swing.*;

public class CasaCambio extends JFrame implements ActionListener
{
	JButton beua, beuro, bcan;
	JLabel imMx, imC, imF, Texto1, Texto2;
	JTextField ctp, ctc;
	String titulo[] = { "Dolares Americanos", "Euros", "Dolares Canadienses" };
	URL rutaIma[];
	int tipo = 0;
	float tipoC[]= {19.25f,22.0f,17.50f};

	public CasaCambio()
	{
		setSize(500, 300);
		setTitle("Casa de cambio Halcones de Jiquilpan");
		setLocationRelativeTo(this);
		setResizable(false);
		URL ruta = getClass().getResource("/Unidad_I/moneda/eua.png");
		rutaIma = new URL[3];
		rutaIma[0] = ruta;
		beua = new JButton(new ImageIcon(ruta));
		ruta = getClass().getResource("/Unidad_I/moneda/europea.png");
		rutaIma[1] = ruta;
		beuro = new JButton(new ImageIcon(ruta));
		ruta = getClass().getResource("/Unidad_I/moneda/canada.png");
		rutaIma[2] = ruta;
		bcan = new JButton(new ImageIcon(ruta));
		JPanel centro = new JPanel(new GridLayout(2, 3));
		beua.addActionListener(this);
		beuro.addActionListener(this);
		bcan.addActionListener(this);
		centro.add(beua);
		centro.add(beuro);
		centro.add(bcan);
		ruta = getClass().getResource("/Unidad_I/moneda/mexico.png");
		imMx = new JLabel(new ImageIcon(ruta));
		centro.add(imMx);
		ruta = getClass().getResource("/Unidad_I/moneda/flechad.png");
		imF = new JLabel(new ImageIcon(ruta));
		centro.add(imF);
		ruta = getClass().getResource("/Unidad_I/moneda/eua.png");
		imC = new JLabel(new ImageIcon(ruta));
		centro.add(imC);
		add(centro, BorderLayout.CENTER);
		Texto1 = new JLabel("Peso");
		Texto1.setFont(new Font("Arial", Font.BOLD, 18));
		Texto2 = new JLabel("Dolares Americanos");
		Texto2.setFont(new Font("Arial", Font.BOLD, 18));
		ctp = new JTextField(20);
		ctp.setFont(new Font("Arial", Font.BOLD, 18));
		ctc = new JTextField(20);
		ctc.setFont(new Font("Arial", Font.BOLD, 18));
		JPanel sur = new JPanel(new GridLayout(2, 2));
		sur.add(Texto1);
		sur.add(Texto2);
		sur.add(ctp);
		sur.add(ctc);
		add(sur, BorderLayout.SOUTH);

		ctp.addKeyListener(new KeyAdapter()
		{
			public void keyPressed(KeyEvent e)
			{
				URL ruta = getClass().getResource("/Unidad_I/moneda/flechad.png");
				imF.setIcon(new ImageIcon(ruta));
			}
		});
		ctc.addKeyListener(new KeyAdapter()
		{
			public void keyPressed(KeyEvent e)
			{
				URL ruta = getClass().getResource("/Unidad_I/moneda/flechai.png");
				imF.setIcon(new ImageIcon(ruta));
			}
		});
		ctp.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				try
				{
					float dinero = Float.parseFloat(ctp.getText());
					float cambio = dePesosA(tipo, dinero);
					ctc.setText(cambio + "");
				}
				catch (NumberFormatException e1)
				{
					JOptionPane.showMessageDialog(null, "Debe de ser numerico");
				}
			}
		});
		ctc.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				try 
				{
					float dinero=Float.parseFloat(ctc.getText());
					float cambio=deTipoaPesos(dinero);
					ctp.setText(cambio+"");
				}
				catch(NumberFormatException e1)
				{
					JOptionPane.showMessageDialog(null, "Debe ser numerico");
				}
			}
		});
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public float dePesosA(int t, float money)
	{
		float res=0.0f;
		res=money/tipoC[t];
		return res;
	}
	
	public float deTipoaPesos(float money)
	{
		float res=0.0f;
		res=money*tipoC[tipo];
		return res;
	}

	public static void main(String[] args)
	{
		new CasaCambio();
	}

	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == beua)
			tipo = 0;
		else
			if (e.getSource() == beuro)
				tipo = 1;
			else
				tipo = 2;
		Texto2.setText(titulo[tipo]);
		imC.setIcon(new ImageIcon(rutaIma[tipo]));
	}
}
