package Unidad_I;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

class Alumnos{
	String nombre;
	int edad;
	char sexo;
	public Alumnos()
	{
		nombre="";
		edad=0;
		sexo=' ';
	}
	public Alumnos(String n, int e, char s)
	{
		nombre=n;
		edad=e;
		sexo=s;
	}
	public String getName()
	{
		return nombre;
	}
	public void setName(String name)
	{
		this.nombre = name;
	}
	public int getEdad()
	{
		return edad;
	}
	public void setEdad(int e)
	{
		this.edad = e;
	}
	public char getSexo()
	{
		return sexo;
	}
	public void setSexo(char s)
	{
		this.sexo = s;
	}
	
}

class cuadroD extends JDialog
{
	JLabel etn,ete,ets;
	JTextField ctn;
	Choice le;
	ButtonGroup bg;
	JRadioButton rb1,rb2;
	Button ba,bc;
	Alumnos A;
	public cuadroD(JFrame f,boolean modal)
	{
		super(f,modal);
		setTitle("Dame los datos del alumno");
		setSize(400,200);
		setLocation(100,100);
		//construir el cuadro de dialogo
		setLayout(new GridLayout(4,2));
		etn=new JLabel("Escribe tu nombre");
		ctn=new JTextField(40);
		add(etn);
		add(ctn);
		ete=new JLabel("Selecciona tu edad");
		le=new Choice();
		for (int i = 17; i < 80; i++)
			le.add(i+"");
		add(ete);
		add(le);
		ets=new JLabel("Selecciona tu sexo");
		bg=new ButtonGroup();
		rb1=new JRadioButton("F",true);
		rb2=new JRadioButton("M");
		bg.add(rb1);
		bg.add(rb2);
		JPanel der=new JPanel(new GridLayout(1,2));
		der.add(rb1);
		der.add(rb2);
		add(ets);
		add(der);
		ba=new Button("Aceptar");
		bc=new Button("Cancelar");
		add(ba);
		add(bc);
		A=new Alumnos();
		ba.addActionListener(new ActionListener(){ 
			public void actionPerformed(ActionEvent e) {
				String nombre=ctn.getText();
				int edad=17+le.getSelectedIndex();
				char s=' ';
				if(rb1.isSelected())
					s='F';
				else
					s='M';
				A=new Alumnos(nombre,edad,s);
				setVisible(false);
				dispose();
			}
		});
		bc.addActionListener(new ActionListener(){ 
			public void actionPerformed(ActionEvent e) {
				A=null;
				setVisible(false);
				dispose();
			}
		});
		//setVisible(true);  NO PUEDE HABER 2 SENTENCIAS DE ESTAS IGUALES EN LA CLASE
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		
	}
	
	public Alumnos mostrar()
	{
		setVisible(true);
		return A;
	}
}

public class SusoCuadrosD extends JFrame
{
	JButton b1;
	
	public SusoCuadrosD()
	{
		setSize(800,600);
		b1=new JButton("Pedir Datos");
		add(b1, BorderLayout.NORTH);
		b1.addActionListener(new ActionListener()
			{
			public void actionPerformed(ActionEvent e) {
				cuadroD C=new cuadroD(SusoCuadrosD.this,true);
				Alumnos X=C.mostrar();
				if(X!=null)
					System.out.println(X.getName()+" "+X.getEdad()+" "+X.getSexo());
			}
			});
		
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
	}
	
	public static void main(String [] args)
	{
		new SusoCuadrosD();
	}
}
