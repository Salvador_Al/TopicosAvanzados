package Unidad_I;

import java.applet.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JOptionPane;

public class Graficas extends Applet implements ActionListener
{
	Label et;
	TextField ct;
	Button b1, b2;
	int vec[], pos;

	public void init()
	{
		setSize(800, 600);
		setBackground(Color.LIGHT_GRAY);
		et = new Label("Dame un numero");
		add(et);
		ct = new TextField(5);
		add(ct);
		b1 = new Button("Insertar");
		add(b1);
		b2 = new Button("Reiniciar");
		add(b2);
		b1.addActionListener(this);
		b2.addActionListener(this);
		vec=new int[10];
		pos=-1;
	}

	public void paint(Graphics g)
	{
		// dibujar los cuadritos
		int x = 50, y = 120, width = 75, height = 40;
		for (int i = 0; i < 10; i++)
		{
			g.drawRect(x, y, width, height);
			y += height;
		}
		//escribir los numeros
		x=85;
		y=145;
		for(int i=0; i<=pos; i++)
		{
			g.drawString(vec[i]+"", x, y);
			y+=height;
		}
		//if(pos==9) //ya estan los 10 numeros
		{ //dibujar la grafica
			g.drawLine(200, 500, 700, 500);
			g.drawLine(250, 550, 250, 200);
			Random R=new Random();
			x=251;y=500;width=50;
			for (int i = 0; i <= pos; i++)
			{
				g.setColor(new Color(R.nextInt(256),R.nextInt(256),R.nextInt(256)));
				g.fillRect(x, y-vec[i], width, vec[i]);
				x+=width;
			}
		}
	}

	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == b1)
		{
			String texto = ct.getText();
			try
			{
				int num = Integer.parseInt(texto);
				if(num>=0 && num<=100 && (pos+1<vec.length))
				{
					vec[++pos]=num;
				}
			}
			catch (NumberFormatException e1)
			{
				JOptionPane.showMessageDialog(null, "Debe de ser un valor numerico entre 0 y 100");
			}
			ct.setText("");
		}
		else
		{
			pos=-1;
		}
		repaint();
	}
}
