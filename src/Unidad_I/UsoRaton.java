package Unidad_I;

import java.applet.*;
import java.awt.*;
import java.awt.event.*;

//Mouse Listener, MouseMotionListener, MouseWhellListener

class Manejadora extends MouseAdapter
{
	UsoRaton r;
	public Manejadora(UsoRaton u)
	{
		r=u;
	}
	public void MousePressed(MouseEvent e)
	{
		if (e.getButton() == MouseEvent.BUTTON1)
		{
			r.cx = e.getX();
			r.cy = e.getY();
			r.mens = "presionaste el raton " + r.cx + "," + r.cy;
			r.repaint();
		}
	}
}

public class UsoRaton extends Applet
{
	String mens = "";
	int cx = 0, cy = 0;

	public void init()
	{
		setSize(800, 600);
		Manejadora obj=new Manejadora(this);
		addMouseListener(obj);
	}

	public void paint(Graphics g)
	{
		g.drawString(mens, cx, cy);
	}

	/*public void mouseClicked(MouseEvent e)
	{

	}

	public void mousePressed(MouseEvent e)
	{
		if (e.getButton() == 1)
		{
			cx = e.getX();
			cy = e.getY();
			mens = "presionaste el raton " + cx + "," + cy;
			repaint();
		}
	}

	public void mouseReleased(MouseEvent e)
	{

	}

	public void mouseEntered(MouseEvent e)
	{
		cx = e.getX();
		cy = e.getY();
		mens = "Entraste a la ventana " + cx + "," + cy;
		repaint();
	}

	public void mouseExited(MouseEvent e)
	{
		cx = e.getX();
		cy = e.getY();
		mens = "Saliste de la ventana " + cx + "," + cy;
		repaint();
	}*/
}
