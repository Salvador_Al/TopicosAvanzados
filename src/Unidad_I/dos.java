package Unidad_I;

import java.awt.*;
import java.applet.*;

public class dos extends Applet
{

	public void init()
	{
		setSize(800, 600);
		setBackground(Color.LIGHT_GRAY);
	}

	public void paint(Graphics g)
	{
		showStatus("Dentro del paint viendo el uso de Graphics");
		Dimension d = getSize();
		g.drawLine(50, 100, 500, 100);
		g.setColor(Color.red);
		g.drawLine(0, 0, d.width, d.height);
		g.setColor(Color.green);
		g.drawRect(50, 200, 100, 300);
		g.setColor(Color.yellow);
		g.fillRect(200, 250, 300, 100);
		g.setColor(new Color(159, 56, 147, 50));// color r,g,b,trasparencia
		g.fillOval(400, 50, 100, 100);
		int cx[] = { 100, 300, 200 };
		int cy[] = { 400, 400, 300 };
		g.setColor(Color.CYAN);
		g.fillPolygon(cx, cy, cx.length);
	}

}
