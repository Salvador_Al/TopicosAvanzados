package Unidad_I;

import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.util.Random;

public class juegodados extends Applet {
	Image dados[] = new Image[6];
	int ndado1 = 1, ndado2 = 1;
	int monto=200;
	
	Label l1,l2,l3;
	TextField t1,t2,t3;
	
	Button b1;

	public void init() {
		setSize(400, 400);
		setFont(new Font("Arial", Font.ITALIC, 20));
		setLayout(new BorderLayout());
		for (int i = 0; i < dados.length; i++) {
			URL ruta = getClass().getResource("/Unidad_I/dados/im" + (i + 1) + ".jpg");
			dados[i] = getImage(ruta);
		}
		Panel p1=new Panel();
		l1=new Label("Dado 1:");
		l2=new Label("Dado 2:");
		l3=new Label("Monto:");
		t1=new TextField(5);
		t2=new TextField(5);
		t3=new TextField("200",5);
		b1=new Button("Lanzar");
		p1.add(l1);p1.add(t1);p1.add(l2);p1.add(t2);
		add(p1, BorderLayout.NORTH);
		Panel p2=new Panel();
		p2.add(b1);p2.add(l3);p2.add(t3);
		add(p2, BorderLayout.SOUTH);
		b1.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Random r=new Random();
				
					ndado1=1+r.nextInt(5);
					ndado2=1+r.nextInt(5);
				
				
				
				t1.setText(""+ndado1);
				t2.setText(""+ndado2);
				repaint();
				
				if(ndado1==1 && ndado2==1)
					monto+=100;
				else
					monto-=20;
				t3.setText(""+monto);
				
				
			}
		});
		
	}

	@Override
	public void paint(Graphics g) {
		g.drawImage(dados[ndado1-1], 50, 100, 100, 100, this);
		g.drawImage(dados[ndado2-1], 200, 100, 100, 100, this);
	}
}
