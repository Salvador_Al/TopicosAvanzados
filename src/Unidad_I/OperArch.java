package Unidad_I;

import java.awt.*;
import java.awt.event.*;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.applet.*;

public class OperArch extends Applet implements ActionListener
{
	Label etr;
	TextField ruta;
	Button b1, b2, b3, b4;
	TextArea areat;
	FileDialog fd;

	public void init()
	{
		setSize(800, 600);
		setLayout(new BorderLayout());
		etr = new Label("ruta:\\\\");
		ruta = new TextField(80);
		Panel arriba = new Panel();
		arriba.add(etr);
		arriba.add(ruta);
		add(arriba, BorderLayout.NORTH);
		b1 = new Button("Abrir");
		b2 = new Button("Borrar");
		b3 = new Button("Guardar");
		b4 = new Button("Salir");
		Panel izq = new Panel();
		izq.setLayout(new GridLayout(6, 1));
		izq.add(b1);
		izq.add(b2);
		izq.add(b3);
		izq.add(b4);
		add(izq, BorderLayout.WEST);
		areat = new TextArea("", 10, 10, TextArea.SCROLLBARS_BOTH);
		add(areat, BorderLayout.CENTER);
		fd = new FileDialog(new Frame(), "Selecciona un archivo", FileDialog.LOAD);
		b1.addActionListener(this);
		b2.addActionListener(this);
		b3.addActionListener(this);
		b4.addActionListener(this);
	}

	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource()==b1)
		{
		fd.setVisible(true);
		String rutas = fd.getDirectory() + fd.getFile();
		ruta.setText(rutas);
		String contenido = Leer(rutas);
		if(contenido!=null)
			areat.setText(contenido);
		}
		else
			if(e.getSource()==b2)
			{
				areat.setText("");
				ruta.setText("");
			}
			else
			{
				if(e.getSource()==b3)
				{ //grabar un Archivo
					String r=ruta.getText();
					if(r.compareTo("")!=0)
					{ //si si esta escrito algo
						String cont=areat.getText();
						Grabar(cont,r);
						areat.setText("");
						ruta.setText("");
					}
					else
					{ //no esta escrita la ruta
						FileDialog fd1=new FileDialog(new Frame(), "Dame el nombre del archivo a grabar", FileDialog.SAVE);
						fd1.setVisible(true);
						String name=fd1.getFile();
						String dir=fd1.getDirectory();
						ruta.setText(dir+name);
						String cont=areat.getText();
						Grabar(cont,(dir+name));
						areat.setText("");
						ruta.setText("");
					}
				}
				else
				{ //Cierra el programa
					System.exit(0);
				}
			}
	}
	
	public void Grabar(String c, String r)
	{
		FileOutputStream arch=null;
		try
		{
			arch=new FileOutputStream(r);
			arch.write(c.getBytes());
			arch.close();
		}
		catch(IOException e)
		{
			
		}
	}

	public String Leer(String r)
	{
		FileInputStream arch = null;
		String cont = "";
		byte buffer[] = new byte[32];
		int numLei = 0;
		try
		{
			arch = new FileInputStream(r);
			do
			{
				numLei = arch.read(buffer);
				cont += new String(buffer, 0, numLei);

			}
			while (numLei == 32);
			arch.close();
		}
		catch (IOException e)
		{
			return null;
		}
		return cont;
	}
}
