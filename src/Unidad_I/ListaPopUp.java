package Unidad_I;

import java.awt.*;
import java.applet.*;
import java.awt.event.*;

public class ListaPopUp extends Applet
{
	Choice lista;
	int numFig=0;
	
	public void init()
	{
		setSize(800,600);
		lista=new Choice();
		lista.add("<<Figuras Geometricas>>");
		lista.add("Circulo Rojo");
		lista.add("Rectangulo Azul");
		lista.add("Triangulo Verde");
		lista.add("Cuadrado Rosa");
		add(lista);
		lista.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
			numFig=lista.getSelectedIndex();
			showStatus("Seleccionaste: "+lista.getSelectedItem());
			repaint();
			}
		});
	}
	
	public void paint(Graphics g)
	{
		switch(numFig)
		{
			case 1: //circulo
				g.setColor(Color.red);
				g.fillOval(150, 200, 200, 200);
				break;
			case 2://rectangulo
				g.setColor(Color.BLUE);
				g.fillRect(150, 200, 300, 200);
				break;
			case 3://triangulo
				g.setColor(Color.GREEN);
				int cx[] = {150,350,250};
				int cy[] = {300,300,150};
				g.fillPolygon(cx, cy, 3);
				break;
			case 4: //cuadrado
				g.setColor(Color.PINK);
				g.fillRect(150, 200, 200, 200);
				break;
		}
	}
}
