package Unidad_I;

import java.awt.*;
import java.applet.*;

public class posAbsoluto extends Applet
{
	Button b1, b2, b3, b4;

	public void init()
	{
		setLayout(null);
		setSize(600, 400);
		b1 = new Button("Boton 1");
		b1.setBounds(1, 1, 100, 50);
		add(b1);
		b2 = new Button("Boton 2");
		b2.setBounds(500, 1, 100, 50);
		add(b2);
		b3 = new Button("Boton 3");
		b3.setBounds(500, 300, 100, 50);
		add(b3);
		b4 = new Button("Boton 4");
		b4.setBounds(1, 300, 100, 50);
		add(b4);
	}
}
