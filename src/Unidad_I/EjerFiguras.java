package Unidad_I;

import java.applet.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

//panel -> contenedor que se deriva de la clase container
//agrupar diferrentes componentes en un solo elemento
//este contenedor puede tener su propio gestor de organizacion
public class EjerFiguras extends Applet implements ActionListener
{
	Button bR, bC, bCa;
	Color color;
	int numFig;
	Label et;
	Label etr, etg, etb;
	TextField ctr, ctg, ctb;
	Button act;

	public void init()
	{
		setSize(800, 600);
		setLayout(new BorderLayout()); // con este componente, solo se puede
										// poner un solo componente
		et = new Label("Selecciona una figura y modifia el color", Label.CENTER);
		et.setFont(new Font(Font.SANS_SERIF, Font.ITALIC | Font.BOLD, 16));
		add(et, BorderLayout.NORTH);
		bR = new Button("Rectangulo");
		bC = new Button("Circulo");
		bCa = new Button("color aleatiorio");
		Panel gb = new Panel(); // flowLayout
		gb.setLayout(new GridLayout(6, 1));
		gb.add(bR);
		gb.add(bC);
		gb.add(bCa);
		add(gb, BorderLayout.WEST);
		color = Color.BLUE;
		numFig = 0;
		bR.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				numFig = 1;
				repaint();
			}
		});
		bC.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				numFig = 2;
				repaint();
			}
		});
		bCa.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				int r, g, b;
				Random R = new Random();
				r = R.nextInt(256);
				g = R.nextInt(256);
				b = R.nextInt(256);
				color = new Color(r, g, b);
				repaint();
			}
		});

		etr = new Label("Rojo");
		etg = new Label("Verde");
		etb = new Label("Azul");
		ctr = new TextField("0", 5);
		ctg = new TextField("0", 5);
		ctb = new TextField("0", 5);
		act = new Button("Actualizar");
		Panel p2 = new Panel();
		p2.add(etr);
		p2.add(ctr);
		p2.add(etg);
		p2.add(ctg);
		p2.add(etb);
		p2.add(ctb);
		p2.add(act);
		add(p2, BorderLayout.SOUTH);
		act.addActionListener(this);
	}

	@Override
	public void paint(Graphics g)
	{
		g.setColor(color);
		switch (numFig) {
			case 1:
				g.fillRect(150, 150, 200, 150);
				break;
			case 2:
				g.fillOval(150, 150, 200, 200);
				break;
		}
	}

	public void actionPerformed(ActionEvent e)
	{
		String v1 = ctr.getText(), v2 = ctg.getText(), v3 = ctb.getText();
		int r = 0, g = 0, b = 0;
		try
		{
			r = Integer.parseInt(v1);
			g = Integer.parseInt(v2);
			b = Integer.parseInt(v3);
			color = new Color(r, g, b);
		}
		catch (NumberFormatException e1)
		{
			color = Color.BLUE;
		}
		repaint();
	}
}
