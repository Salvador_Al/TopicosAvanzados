package Unidad_I;


import java.awt.*;
import java.awt.event.WindowListener;
import java.net.*;
import javax.swing.*;

public class Sventana extends JFrame {
	ImageIcon img;
	Image imagen;
	JButton b1;
	public Sventana(String titulo) {
		setSize(800, 600);
		setBackground(Color.LIGHT_GRAY);
		setLocation(300,100);
		//setLocationByPlatform(true);
		setResizable(false);
		setTitle(titulo);
		URL ruta=getClass().getResource("/UnidadIGUI/imag/1.jpg");
		img=new ImageIcon(ruta);
		//imagen=new ImageIcon(ruta).getImage();
		Toolkit tk=getToolkit();
		imagen=tk.getImage(ruta);
		b1=new JButton("Push me",img);
		b1.setMnemonic('P');
		b1.setToolTipText("Presioname y te muestro una sorpresa bb");//aparece mensaje al estar sobre el
		add(b1);
		
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void paint(Graphics g) {
		super.paint(g);
		g.fillRect(100, 100, 200, 200);
		img.paintIcon(this, g, 200, 200);
		g.drawImage(imagen, 400, 100, 100, 100, this);
	}
	
	public static void main(String[] args) {
		new Sventana("Primer programa  en Swing");
	}
}
