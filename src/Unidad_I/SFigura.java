package Unidad_I;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.net.*;

public class SFigura extends JFrame implements ActionListener
{
	JButton b1, b2, b3, b4;
	int numIn=0;
	
	JMenuBar barram;
	
	public void menu()
	{
		barram=new JMenuBar();
		this.setJMenuBar(barram);
		JMenu op,Ay;
		op=new JMenu("Figura");
		barram.add(op);
		Ay=new JMenu("Ayuda");
		barram.add(Ay);
		JMenuItem Op1,Op2,Op3;
		URL ruta=getClass().getResource("/Unidad_I/imag/pencil.png");
		Op1=new JMenuItem("Circulo",new ImageIcon(ruta));
		Op1.setMnemonic('C');
		Op1.setToolTipText("Dibuja un circulo rojo");
		Op1.setAccelerator(KeyStroke.getKeyStroke('C', KeyEvent.VK_CONTROL));
		ruta = getClass().getResource("/Unidad_I/imag/guitar.png");
		Op2=new JMenuItem("Cuadrado");
		Op2.setMnemonic('a');
		Op2.setToolTipText("Dibuja un cuadrado Azul");
		JMenuItem s1,s2;
		s1=new JMenuItem("Sin relleno", new ImageIcon(ruta));
		ruta = getClass().getResource("/Unidad_I/imag/rose.png");
		s2=new JMenuItem("Con relleno", new ImageIcon(ruta));
		ruta = getClass().getResource("/Unidad_I/imag/s.png");
		Op3=new JMenuItem("Salir",new ImageIcon(ruta));
		//JSeparator sep=new JSeparator();
		Op3.setAccelerator(KeyStroke.getKeyStroke('Q', KeyEvent.VK_ALT));
		Op2.add(s1);
		Op2.add(s2);
		op.add(Op1);
		op.add(Op2);
		op.addSeparator();
		op.add(Op3);
		
		Op3.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				System.exit(0);
			}
		});
		Op1.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				numIn=1;
				repaint();
			}
		});
		s1.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				numIn=5;
				repaint();
			}
		});
	}
	
	public SFigura()
	{
		setSize(800,600);
		setLayout(new BorderLayout());
		menu();
		setBackground(Color.LIGHT_GRAY);
		JPanel izq=new JPanel(new GridLayout(6,2));
		URL ruta=getClass().getResource("/Unidad_I/imag/circulo.jpg");
		b1=new JButton(new ImageIcon());
		izq.add(b1);
		ruta=getClass().getResource("/Unidad_I/imag/rectangulo.jpg");
		b2=new JButton(new ImageIcon());
		izq.add(b2);
		ruta=getClass().getResource("/Unidad_I/imag/triangulo.jpg");
		b3=new JButton(new ImageIcon());
		izq.add(b3);
		ruta=getClass().getResource("/Unidad_I/imag/rectred.jpg");
		b4=new JButton(new ImageIcon());
		izq.add(b4);
		add(izq,BorderLayout.WEST);
		b1.addActionListener(this);
		b2.addActionListener(this);
		b3.addActionListener(this);
		b4.addActionListener(this);
		
		
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void paint(Graphics g)
	{
		super.paint(g);
		switch(numIn)
		{
			case 1:
				g.setColor(Color.RED);
				g.fillOval(300, 150, 200, 200);
				break;
			case 2:
				g.setColor(Color.BLUE);
				g.fillRect(300, 150, 200, 200);
				break;
			case 3:
				int cx[]= {300,500,400};
				int cy[]= {300,300,150};
				g.setColor(Color.YELLOW);
				g.fillPolygon(cx,cy,3);
				break;
			case 4:
				g.setColor(Color.RED);
				g.fillRoundRect(300, 150, 200, 150,30,30);
				break;
			case 5:
				g.setColor(Color.BLUE);
				g.fillRect(300, 150, 200, 200);
				break;
			case 6:
				g.setColor(Color.BLUE);
				g.drawRect(300, 150, 200, 200);
				break;
		}
	}

	public void actionPerformed(ActionEvent e)
	{
		if(e.getSource() == b1)
		{
			numIn=1;
		}
		else
			if(e.getSource() == b2)
			{
				numIn=2;
			}
			else
				if(e.getSource() == b3)
				{
					numIn=3;
				}
				else
					if(e.getSource() == b4)
					{
						numIn=4;
					}
	}
	
	public static void main(String[] args)
	{
		new SFigura();
	}
}