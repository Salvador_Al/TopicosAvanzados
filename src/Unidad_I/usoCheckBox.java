package Unidad_I;

import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.applet.*;

public class usoCheckBox extends Applet implements ItemListener
{
	String pregunta,respuesta;
	Checkbox cb1,cb2,cb3,cb4,cb5;
	
	public void init()
	{
		setSize(800,600);
		pregunta="Cuales Sistemas Operativos Has Utilizado?";
		respuesta="Los sistemas operativos utilizados son: ";
		setLayout(new BorderLayout());
		cb1=new Checkbox("Windows ");
		cb2=new Checkbox("Linux ");
		cb3=new Checkbox("Mac OS ");
		cb4=new Checkbox("Solaris ");
		cb5=new Checkbox("Firefox OS ");
		Panel izq=new Panel();
		izq.setLayout(new GridLayout(8,1));
		izq.add(cb1);
		izq.add(cb2);
		izq.add(cb3);
		izq.add(cb4);
		izq.add(cb5);
		add(izq,BorderLayout.WEST);
		//escuchas
		cb1.addItemListener(this);
		cb2.addItemListener(this);
		cb3.addItemListener(this);
		cb4.addItemListener(this);
		cb5.addItemListener(this);
		
		
	}
	public void paint(Graphics g)
	{
		g.setFont(new Font("Arial",Font.BOLD,25));
		g.drawString(pregunta, 150, 100);
		g.setColor(Color.BLUE);
		g.drawString(respuesta, 150, 200);
	}
	
	public void itemStateChanged(ItemEvent e)
	{
		respuesta="Los sistemas operativos utilizados son: ";
		if(cb1.getState())
			respuesta+=cb1.getLabel();
		if(cb2.getState())
			respuesta+=cb2.getLabel();
		if(cb3.getState())
			respuesta+=cb3.getLabel();
		if(cb4.getState())
			respuesta+=cb4.getLabel();
		if(cb5.getState())
			respuesta+=cb5.getLabel();
		repaint();
	}
}
