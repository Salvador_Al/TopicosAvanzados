package Unidad_I;

import java.awt.*;
import java.applet.*;
import java.awt.event.*;

public class EjerListas extends Applet implements ItemListener, ActionListener {
	
	Choice lp;
	List li;
	String mens;
	
	public void init() {
		setSize(1000,600);
		mens="Selecciona un Sistema Operativo � Navegador";
		lp=new Choice();
		lp.add("<<Sistemas Operativos>>");
		lp.add("Windows 10");
		lp.add("Fedora 18");
		lp.add("Mac OS");
		lp.add("Solaris");
		li=new List(4);//Numero de elementos que se van a visualizar
		li.add("<<Navegadores>>");
		li.add("Safari");
		li.add("Opera");
		li.add("Microsoft EDGE");
		li.add("Mozzila Firefox");
		add(lp);
		add(li);
		lp.addItemListener(this);
		li.addActionListener(this);
	}
	
	
	public void paint(Graphics g) {
		g.setFont(new Font(Font.SANS_SERIF, Font.ITALIC,20));
		g.drawString(mens, 200, 200);
	}

	public void actionPerformed(ActionEvent e) {
		String nav=li.getSelectedItem();//Devuelve el texto
		switch(nav) {
		case "Safari":
			lp.select(3);//posicion o texto
			break;
		case "Opera":
			lp.select(4);//posicion o texto
			break;
		case "Microsoft EDGE":
			lp.select(1);//posicion o texto
			break;
		case "Mozzila Firefox":
			lp.select(2);//posicion o texto
			break;
		}
		mens="Al Navegador "+li.getSelectedItem()+" le corresponde el Sistema Operativo "+ nav;
		repaint();
		
	}
	
	public void itemStateChanged(ItemEvent e) {
		String os=lp.getSelectedItem();//Devuelve el texto
		switch(os) {
		case "Windows 10":
			li.select(3);//posicion o texto
			break;
		case "Fedora 18":
			li.select(4);//posicion o texto
			break;
		case "Mac OS":
			li.select(1);//posicion o texto
			break;
		case "Solaris":
			li.select(2);//posicion o texto
			break;
		}
		mens="Al Sistema Operativo "+os+" le corresponde el navegador de "+ li.getSelectedItem();
		repaint();
		
	}
	

}
