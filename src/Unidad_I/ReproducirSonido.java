package Unidad_I;

import java.applet.*;
import java.awt.*;
import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.*;

public class ReproducirSonido extends Applet
{
	AudioClip S;
	Button bp, bs, bl;
	public void init()
	{
		setSize(400,200);
		URL ruta=getClass().getResource("/Unidad_I/sonidos/spacemusic.au");
		S=getAudioClip(ruta);
		bp=new Button("Play");
		bs=new Button("Stop");
		bl=new Button("Loop");
		add(bp);
		add(bs);
		add(bl);
		bp.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				S.play();
			}
		});
		bs.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				S.stop();
			}
		});
		bl.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				S.loop();
			}
		});
	}
}
