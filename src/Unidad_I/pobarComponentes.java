package Unidad_I;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.applet.*;

public class pobarComponentes extends Applet {
	TextArea areat;
	FileDialog selector;
	Button b1;
	public void init() {
		setSize(800,600);
		setLayout(new BorderLayout());
		
		areat=new TextArea("", 10,10, TextArea.SCROLLBARS_BOTH);
		add(areat, BorderLayout.CENTER);
		b1=new Button("Selecciona un Archivo");
		add(b1,BorderLayout.NORTH);
		selector=new FileDialog(new Frame(),"Selecciona un archivo");
		areat.setFont(new Font("Arial",Font.PLAIN,20));
		b1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				selector.setVisible(true);
				String dir=selector.getDirectory();
				String nom=selector.getFile();
				areat.append(dir+nom+"\n");
			}
		});
	}
}
