package Unidad_I;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.*;

public class SUsoSlider extends JFrame {
	JSlider regla;
	int valor=200;

	public SUsoSlider() {
		setSize(800, 600);
		regla = new JSlider(JSlider.HORIZONTAL, 0, 400, 200);// ubicacion minimo maximo posicion
		add(regla, BorderLayout.NORTH);
		regla.setMajorTickSpacing(50);// graduacion
		regla.setMinorTickSpacing(5);// palitos
		regla.setPaintTicks(true);
		regla.setPaintLabels(true);// pinte etiquetas
		regla.setBackground(Color.red);
		regla.setForeground(Color.lightGray);
		regla.addChangeListener(new ChangeListener() {// escucha
			public void stateChanged(ChangeEvent e) {
				valor=regla.getValue();
				repaint();
			}
		});
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void paint(Graphics g) {
		super.paint(g);
		g.setFont(new Font("Arial",Font.BOLD,100));
		g.drawString(valor+"", 300, 300);
	}
	public static void main(String[] args) {
		new SUsoSlider();
	}
}
