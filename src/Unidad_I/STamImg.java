package Unidad_I;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import java.net.*;
public class STamImg extends JFrame implements ActionListener{
	Image img;
	JButton b1,b2,b3;
	int tam=1;
	
	public STamImg() {
		setSize(800,600);
		
		JPanel bajo=new JPanel();
		bajo.setLayout(new GridLayout(1,3));
		b1=new JButton("Grande");
		b2=new JButton("Mediano");
		b3=new JButton("Chico");
		bajo.add(b1);
		bajo.add(b2);
		bajo.add(b3);
		add(bajo, BorderLayout.SOUTH);
		b1.addActionListener(this);
		b2.addActionListener(this);
		b3.addActionListener(this);
		URL ruta=getClass().getResource("/UnidadIGUI/imag/1.jpg");
		img=new ImageIcon(ruta).getImage();		
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public void paint(Graphics g) {
		super.paint(g);
		switch(tam) {
		case 1:
			g.drawImage(img, 300, 100, 300, 300, this);
			break;
		case 2:
			g.drawImage(img, 300, 100, 200, 200, this);
			break;
		case 3:
			g.drawImage(img, 300, 100, 100, 100, this);
			break;
		}
		
	}

	public static void main(String[] args) {
		new STamImg();
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == b1) {
			tam = 1;
		} else if (e.getSource() == b2) {
			tam = 2;
		} else if (e.getSource() == b3) {
			tam = 3;
		}
		repaint();
	}
}
