package Unidad_I;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Collections;
import java.util.LinkedList;

public class SEjerChirimolla extends JFrame implements ActionListener
{
	JButton vBot[][];
	int valBot[][] = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 0 } };
	JButton br;
	JLabel Lm;
	int movi=0;

	public SEjerChirimolla()
	{
		setSize(400, 400);
		JPanel mov=new JPanel(new FlowLayout(FlowLayout.RIGHT));
		Lm=new JLabel("Movimientos: 0");
		Lm.setFont(new Font("Aria",Font.BOLD,20));
		add(Lm,BorderLayout.NORTH);
		setResizable(false);
		setLocation(200, 100);
		setTitle("Ejercicio de la Chirimolla");
		vBot = new JButton[3][3];
		JPanel cuadricula = new JPanel(new GridLayout(3, 3));
		for (int r = 0; r < vBot.length; r++)
			for (int c = 0; c < vBot.length; c++)
			{
				vBot[r][c] = new JButton(valBot[r][c] + "");
				vBot[r][c].setFont(new Font("Arial", Font.BOLD, 22));
				vBot[r][c].setEnabled(false);
				vBot[r][c].setBackground(Color.cyan);
				cuadricula.add(vBot[r][c]);
				vBot[r][c].addActionListener(this);
			}
		vBot[2][2].setText("");
		vBot[2][2].setBackground(Color.black);
		add(cuadricula, BorderLayout.CENTER);
		br = new JButton("Revolver");
		br.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				LinkedList<Integer> nums = new LinkedList<Integer>();
				for (int i = 1; i <= 8; i++)
					nums.add(i);
				nums.add(0);
				Collections.shuffle(nums);
				int k = 0;
				for (int r = 0; r < valBot.length; r++)
					for (int c = 0; c < valBot[0].length; c++)
					{
						valBot[r][c] = nums.get(k++);
						if (valBot[r][c] == 0)
						{
							vBot[r][c].setText("");
							vBot[r][c].setBackground(Color.black);
						}
						else
						{
							vBot[r][c].setText(valBot[r][c] + "");
							vBot[r][c].setBackground(Color.cyan);
						}
						vBot[r][c].setEnabled(true);
					}
				br.setEnabled(false);
			}
		});
		add(br, BorderLayout.SOUTH);

		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args)
	{
		new SEjerChirimolla();
	}

	public void actionPerformed(ActionEvent e)
	{
		// en que posicion esta el boton que precione
		for (int r = 0; r < vBot.length; r++)
			for (int c = 0; c < vBot.length; c++)
			{
				if (e.getSource() == vBot[r][c])
				{
					int val = mePuedoMover(r, c - 1);
					if (val == 0)
					{ // cambio los valores
						cambiar(r,c,r,c-1);
					}
					else
					{
						val = mePuedoMover(r, c + 1);
						if (val == 0)
						{  //Cambiar los valores
							cambiar(r,c,r,c+1);
						}
						else
						{
							val = mePuedoMover(r - 1, c);
							if (val == 0)
							{   //Cambiar los valores
								cambiar(r,c,r-1,c);
							}
							else
							{
								val = mePuedoMover(r + 1, c);
								if(val==0)
								{  //Cambia los valores
									cambiar(r,c,r+1,c);
								}
								else
								{ //No me puedo mover
									JOptionPane.showMessageDialog(null, "No te puedes mover");
									return;
								}
							}
						}
					}
				}
				
				//verificar si gano
				boolean gane=verificarGane();
				if(gane)
				{ //desavilitar los botones
					for (int i = 0; i < vBot.length; i++)
						for (int j = 0; j < vBot.length; j++)
						{
							vBot[i][j].setEnabled(false);
						}
					JOptionPane.showMessageDialog(null, "Ganaste!");
					br.setEnabled(true);
					movi=0;
					Lm.setText("Movimientos: 0");
				}
			}// fin del for
	}
	// fin del metodo
	
	public boolean verificarGane()
	{
		int k=1;
		for (int r = 0; r < valBot.length; r++)
			for (int c = 0; c < valBot[0].length; c++)
			{
				if(valBot[r][c]==k)
				{
					k++;
					if(k==9) 
						return true;
				}
				else
					return false;
			}
		return true;
	}
	
	public void cambiar(int io,int jo, int in, int jn)
	{
		int temp=valBot[io][jo];
		valBot[io][jo]=valBot[in][jn];
		valBot[in][jn]=temp;
		vBot[io][jo].setText("");
		vBot[io][jo].setBackground(Color.black);
		vBot[in][jn].setText(valBot[in][jn]+"");
		vBot[in][jn].setBackground(Color.cyan);
		movi++;
		Lm.setText("Movimientos: "+movi);
	}
	
	public int mePuedoMover(int r, int c)
	{
		try
		{
			int num = valBot[r][c];
			return num;
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
			return -1;
		}
	}
}