package Unidad_I;

import javax.*;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.*;
import java.awt.event.*;

public class SConversorGrados extends JFrame {
	JSlider s1, s2;
	JTextField ct1, ct2;
	JLabel et1, et2;

	public SConversorGrados() {
		setSize(300, 500);
		JPanel p1 = new JPanel(new GridLayout(1,2));
		TitledBorder borde = new TitledBorder("Conversor");
		borde.setTitleJustification(TitledBorder.CENTER);
		p1.setBorder(borde);
		JPanel pi = new JPanel(new BorderLayout());
		TitledBorder bc=new TitledBorder("Centigrados");
		pi.setBorder(bc);
		s1=new JSlider(JSlider.VERTICAL,0,100,0);
		s1.setMajorTickSpacing(20);
		s1.setMinorTickSpacing(5);
		s1.setPaintTicks(true);
		s1.setPaintLabels(true);
		pi.add(s1);
		JPanel pd = new JPanel(new BorderLayout());
		TitledBorder bf=new TitledBorder("Farenheit");
		pd.setBorder(bf);
		s2=new JSlider(JSlider.VERTICAL,32,212,32);
		s2.setMajorTickSpacing(20);
		s2.setMinorTickSpacing(5);
		s2.setPaintTicks(true);
		s2.setPaintLabels(true);
		pd.add(s2);
		p1.add(pi); p1.add(pd);
		add(p1);
		//
		et1=new JLabel("C");
		et2=new JLabel("F");
		ct1=new JTextField("0",5);
		ct2=new JTextField("32",5);
		JPanel abajo=new JPanel();
		TitledBorder tbx=new TitledBorder("");
		abajo.setBorder(tbx);
		abajo.add(et1); abajo.add(ct1);
		abajo.add(et2); abajo.add(ct2);
		add(abajo,BorderLayout.SOUTH);
		//EVENTOS
		s1.addChangeListener(new ChangeListener() 
			{
				public void stateChanged(ChangeEvent e) 
				{
					int vc=s1.getValue();
					ct1.setText(vc+"");
					double res=vc*(9/5)+32;
					ct2.setText(((int)res)+"");
					s2.setValue((int)res);
				}
			});
		s2.addChangeListener(new ChangeListener() 
		{
			public void stateChanged(ChangeEvent e) 
			{
				int vf=s2.getValue();
				ct2.setText(vf+"");
				double res=(vf-32)*(5.0/9.0);
				ct1.setText(((int)res)+"");
				s1.setValue((int)res);
			}
		});
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		new SConversorGrados();
	}
}
