package Unidad_I;

import java.awt.*;
import java.applet.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class usoCheckBoxRadio extends Applet implements ItemListener
{
	String pregunta,respuesta;
	Checkbox cb1,cb2,cb3,cb4,cb5;
	CheckboxGroup cbg;
	public void init()
	{
		setSize(800,600);
		pregunta="Cuales Sistemas Operativo Prefieres mas?";
		respuesta="Uso Mas: ";
		setLayout(new BorderLayout());
		cbg=new CheckboxGroup();
		cb1=new Checkbox("Windows ",cbg,true); //el ultimo que este seleccinado es el correspondiente, pero se le pone false, para que esten deseleccionados predeterminadamente
		respuesta+=cb1.getLabel();
		cb2=new Checkbox("Linux ",cbg,false);
		cb3=new Checkbox("Mac OS ",cbg,false);
		cb4=new Checkbox("Solaris ",cbg,false);
		cb5=new Checkbox("Firefox OS ",cbg,false);
		Panel izq=new Panel();
		izq.setLayout(new GridLayout(8,1));
		izq.add(cb1);
		izq.add(cb2);
		izq.add(cb3);
		izq.add(cb4);
		izq.add(cb5);
		add(izq,BorderLayout.WEST);
		//escuchas
		cb1.addItemListener(this);
		cb2.addItemListener(this);
		cb3.addItemListener(this);
		cb4.addItemListener(this);
		cb5.addItemListener(this);
		
		
	}
	public void paint(Graphics g)
	{
		g.setFont(new Font("Arial",Font.BOLD,25));
		g.drawString(pregunta, 150, 100);
		g.setColor(Color.BLUE);
		g.drawString(respuesta, 150, 200);
	}
	
	public void itemStateChanged(ItemEvent e)
	{
		respuesta="Uso Mas: ";
		Checkbox temp=cbg.getSelectedCheckbox();
		respuesta+=temp.getLabel();
		repaint();
	}
}
