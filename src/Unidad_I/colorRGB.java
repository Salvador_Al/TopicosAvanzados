package Unidad_I;
import java.awt.*;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.applet.*;
public class colorRGB extends Applet implements AdjustmentListener{
	Scrollbar scbr, scbg,scbb;
	Color color=new Color(0,0,0);
	public void init() {
		setSize(800,600);
		setLayout(new BorderLayout());
		scbr=new Scrollbar(Scrollbar.VERTICAL,0,5,0,255);
		scbg=new Scrollbar(Scrollbar.VERTICAL,0,5,0,255);
		scbb=new Scrollbar(Scrollbar.VERTICAL,0,5,0,255);
		Panel p=new Panel(new GridLayout(1,3));
		p.add(scbr);p.add(scbg);p.add(scbb);
		add(p,BorderLayout.WEST);
		scbr.addAdjustmentListener(this);
		scbg.addAdjustmentListener(this);
		scbb.addAdjustmentListener(this);

	}
	
	public void paint(Graphics g) {
		g.setColor(color);
		g.fillOval(150, 100, 400, 400);
	}

	public void adjustmentValueChanged(AdjustmentEvent e) {
		color=new Color(scbr.getValue(),scbg.getValue(),scbb.getValue());
		repaint();
		
	}
}
