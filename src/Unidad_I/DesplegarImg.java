package Unidad_I;

import java.applet.*;
import java.net.*;
import java.awt.*;

public class DesplegarImg extends Applet
{
	Image imagen;   //JPG PNG GIF
	public void init()
	{
		setSize(800,600);
		//Cargar la imagen
		URL ruta=getClass().getResource("/Unidad_I/imag/MUSTANG.JPG");
		imagen=getImage(ruta);
	}
	
	public void paint(Graphics g)
	{
		g.drawImage(imagen, 0, 0,800,600,this);
	}
}
