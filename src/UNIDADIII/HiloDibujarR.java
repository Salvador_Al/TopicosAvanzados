package UNIDADIII;

import java.awt.Color;
import java.awt.Graphics;
import java.util.List;
import java.util.Random;

import javax.swing.SwingWorker;


public class HiloDibujarR extends SwingWorker<Void, String>
{
	DibujarRectangulos dr;
	public HiloDibujarR(DibujarRectangulos r)
	{
		dr=r;
	}

	protected Void doInBackground() throws Exception
	{
		Random R=new Random();
		while (dr.isEnabled())
		{
			int x=80+R.nextInt(dr.getWidth()-80);
			int y=R.nextInt(dr.getHeight());
			
			punto p=new punto();
			p.x=x;p.y=y;
			dr.Coord.add(p);
			
			String pos=(x+","+y);
			publish(pos);
			try
			{
				Thread.sleep(1000);
			}
			catch (InterruptedException e)
			{
			}
			dr.repaint();
		}
		return null;
	}

	protected void process(List<String> L)
	{
		String dato=L.get(L.size()-1);
		dr.at.append(dato+"\n");
	}
	
	public void dibujar(Graphics g,int x, int y)
	{
		Random R=new Random();
		g.setColor(new Color((R.nextInt(255)),(R.nextInt(255)),(R.nextInt(255))));
		g.fillRect(x, y, 100, 60);
	}
}