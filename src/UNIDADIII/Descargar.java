package UNIDADIII;

import java.util.Calendar;

class Trailer
{
	private int cajaAguacate[], pos;

	public void Llenar(int tam)
	{
		cajaAguacate = new int[tam];
		for (int i = 1; i <= tam; i++)
			cajaAguacate[i - 1] = i;
		pos = 0;
	}

	synchronized public int obtenerCaja()
	{
		try
		{
			return cajaAguacate[pos++];
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
			return -1;
		}
	}
}

class Cargador extends Thread
{
	Trailer T;

	public Cargador(String name, Trailer t)
	{
		T = t;
		start();
	}

	public void run()
	{
		int caja = 0;
		while (caja != -1)
		{
			caja = T.obtenerCaja();
			if (caja != -1)
			{
				System.out.println("Descargo " + getName() + " la caja numero " + caja);
			}
			try
			{
				Thread.sleep(100);
			}
			catch (InterruptedException e)
			{

			}
		}
	}
}

public class Descargar
{
	public static void main(String[] args)
	{
		Trailer meche = new Trailer();
		meche.Llenar(100);
		Calendar tiempoi=Calendar.getInstance();
		Cargador c[] = new Cargador[100];
		for (int i = 0; i < c.length; i++)
			c[i] = new Cargador("Cargador " + (i + 1), meche);
		for (int i = 0; i < c.length; i++)
		{
			try
			{
				c[i].join();
			}
			catch (InterruptedException e)
			{
			}
		}
		Calendar tiempof=Calendar.getInstance();
		System.out.println("Fin de la descarga");
		System.out.println("Tiempo de descarga: "+(tiempof.getTimeInMillis()-tiempoi.getTimeInMillis()));
	}
}
