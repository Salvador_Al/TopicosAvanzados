package UNIDADIII;

import java.util.Random;

class recComp
{
	char vec[];
	int pos;

	public recComp()
	{
		vec = new char[50];
		pos = -1;
		int ascii;
		Random obr=new Random();
		for (int i = 0; i < vec.length; i++)
		{
			do
			{
				ascii=obr.nextInt(123);
				vec[i]=(char)(ascii);
			}
			while(vec[i]<96);
		}
	}

	public void mostrar()
	{
		for (int i = 0; i < vec.length; i++)
			System.out.print(vec[i]+" ");
	}
	public synchronized char getChar()
	{
		char c='!';
		try
		{
			return vec[++pos];
		}
		catch (ArrayIndexOutOfBoundsException e)
		{
			return c;
		}
		
	}
}

class vocales implements Runnable
{
	Thread v;
	recComp ref;
	char voc[]=new char[10];
	int pos=-1;
	
	public vocales(String name, recComp rec)
	{
		v=new Thread(this,name);
		ref=rec;
		v.start();
	}
	
	public void run()
	{
		char ind=0;
		int num=0;
		while(ind!=-1)
		{
			ind = ref.getChar();
			if(ind=='a'||ind=='e'||ind=='i'||ind=='o'||ind=='u')
			{
				pos++;
				if(pos<voc.length)
				{
					voc[pos]=ind;
					System.out.println("Vocal:"+voc[pos]);
					num++;
				}
				else
				{
					voc=new char[voc.length+5];
					voc[pos]=ind;
					System.out.println("Vocal:"+voc[pos]);
					num++;
				}
				System.out.println("se encontraron: "+num+" Vocales");
			}
			try
			{
				Thread.sleep(100);
			}
			catch (InterruptedException e)
			{
			}
		}
	}
}

class consonantes extends Thread
{
	recComp ref;
	char con[]=new char[10];
	int pos=-1;
	
	public consonantes(String name,recComp c)
	{
		super(name);
		ref=c;
		start();
	}
	
	public void run()
	{
		char ind=0;
		int num=0;
		while(ind!=-1)
		{
			ind = ref.getChar();
			if(ind!='a'||ind!='e'||ind!='i'||ind!='o'||ind!='u')
			{
				pos++;
				if(pos<con.length && pos<51)
				{
					con[pos]=ind;
					System.out.println("Consonante:"+ind);
					num++;
				}
				else
				{
					if(pos<51) {
					con=new char[con.length+5];
					con[pos]=ind;
					System.out.println("Consonante:"+ind);
					num++;
					}
					System.out.println("se encontraron: "+num+" Consonantes");
				}
				
			}
			try
			{
				Thread.sleep(100);
			}
			catch (InterruptedException e)
			{
			}
		}
	}
}

public class ExamenChar
{
	public static void main(String[] args)
	{

		System.out.println("Examen Caracteres");
		recComp recurso=new recComp();
		vocales h1=new vocales("Hilo de Vocales", recurso);
		consonantes h2=new consonantes("Hilo Consonantes", recurso);
		System.out.println("Fin del hilo");
	}
}

