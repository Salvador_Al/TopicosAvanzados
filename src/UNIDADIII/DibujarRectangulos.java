package UNIDADIII;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

class punto{
	int x,y;
}

public class DibujarRectangulos extends JFrame
{
	JTextArea at;
	HiloDibujarR h1;
	JButton b1;
	ArrayList<punto>Coord=new ArrayList<punto>();
	
	public DibujarRectangulos()
	{
		setSize(1000,600);
		setTitle("Dibuja rectangulos a travez del swingworker");
		at=new JTextArea(100, 10);
		JScrollPane sp=new JScrollPane(at,JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		at.setFont(new Font("Arial",Font.BOLD,20));
		add(sp,BorderLayout.WEST);
		h1=new HiloDibujarR(this);
		b1=new JButton("Arrancar");
		b1.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent e)
			{
				h1.execute();
			}
		});
		add(b1,BorderLayout.SOUTH);
		
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void paint(Graphics g)
	{
		super.paint(g);
		for (int i = 0; i < Coord.size(); i++)
			h1.dibujar(g, Coord.get(i).x, Coord.get(i).y);
	}
	
	public static void main(String[] args)
	{
		new DibujarRectangulos();
	}
}
