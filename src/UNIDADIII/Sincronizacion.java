package UNIDADIII;

class impresora{
	public void imprimir(String doc)
	{
		System.out.print("["+doc);
		try 
		{
			Thread.sleep(100);
		}
		catch(InterruptedException e)
		{
			
		}
		System.out.println("]");
	}
}

class persona extends Thread
{
	impresora imp;
	public persona(String name, impresora imp)
	{
		super(name);
		this.imp=imp;
	}
	public void run()
	{
		synchronized (imp)
		{
			imp.imprimir(getName());
		}
	}
}

public class Sincronizacion
{
	public static void main(String[] args)
	{
		persona oto,ana,juan;
		impresora HP=new impresora();
		oto=new persona("othoniel",HP);
		ana=new persona("ana",HP);
		juan = new persona("juan",HP);
		oto.start();
		ana.start();
		juan.start();
	}
}
