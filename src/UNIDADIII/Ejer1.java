package UNIDADIII;

import java.util.Random;

class subconjuntos implements Runnable
{
	int vec[];
	Thread hilo;

	public subconjuntos(String name)
	{
		hilo = new Thread(this, name);
		// generar 50 numeros aleatorios
		Random R = new Random();
		vec = new int[50];
		System.out.println("Numeros Aleatorios Generados");
		for (int i = 0; i < vec.length; i++)
		{
			vec[i] = R.nextInt(3);
			System.out.print(vec[i] + " ");
		}
		System.out.println();
		hilo.start();
	}

	public void run()
	{
		int cont = 0;
		for (int i = 0; i < vec.length - 1; i++)
		{
			if (vec[i] == 2 && vec[i + 1] == 1)
			{
				System.out.println(hilo.getName());
				cont++;
			}
			try
			{
				Thread.sleep(100);
			}
			catch (InterruptedException e)
			{
			}

		}
		System.out.println("hay " + cont + " subconjuntos 2,1");
	}

}

class totalTs extends Thread
{
	char caracteres[];

	public totalTs(String name)
	{
		super(name);
		// 30 caracteres de manera aleatoria de a-z
		caracteres = new char[30];
		Random R = new Random();
		int rango = (int) ('z') - (int) ('a');
		for (int i = 0; i < caracteres.length; i++)
		{
			caracteres[i] = (char) (((int) ('a')) + R.nextInt(rango + 1));
			System.out.print(caracteres[i] + " ");
		}
		System.out.println();
		start();
	}

	public void run()
	{
		int totT=0;
		for (int i = 0; i < caracteres.length; i++)
		{
			if(caracteres[i]=='t')
			{
				System.out.println(getName());
				totT++;
			}
			try
			{
				Thread.sleep(50);
			}
			catch (InterruptedException e)
			{
			}
		}
		System.out.println("Total de Ts = "+totT);
	}
}

public class Ejer1
{
	public static void main(String[] args)
	{
		subconjuntos h1 = new subconjuntos("Hilo subc 2,1> ");
		totalTs h2 = new totalTs("<Total Ts> ");
	}
}
