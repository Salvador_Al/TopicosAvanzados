package UNIDADIII;

import java.awt.*;
import java.net.*;
import javax.swing.*;

public class Caballo extends Thread
{
	int x, y;
	Image img;
	carrera ref;

	public Caballo(int cx, int cy, carrera r)
	{
		x=cx;
		y=cy;
		URL ruta=getClass().getResource("/UNIDADIII/Caballos/caballo.gif");
		img=new ImageIcon(ruta).getImage();
		ref=r;
	}
	
	public void dibujar(Graphics g)
	{
		g.drawImage(img, x, y, 150,100, ref);
	}
}
