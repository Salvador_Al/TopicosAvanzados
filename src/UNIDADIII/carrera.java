package UNIDADIII;

import java.awt.*;
import java.net.*;
import javax.swing.*;

public class carrera extends JPanel
{
	JFrame vent;
	Image fondo;
	Caballo colorado;
	public carrera()
	{
		vent=new JFrame("Hipodromo Hermanos Rodriguez");
		vent.setSize(800,600);//este es el tamano del panel
		setSize(800,600);
		URL ruta=getClass().getResource(""+"/UNIDADIII/Caballos/pista.jpg");
		fondo=new ImageIcon(ruta).getImage();
		vent.add(this);
		colorado=new Caballo(0, 200, this);
		
		vent.setVisible(true);
		vent.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args)
	{
		new carrera();
	}
	
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.drawImage(fondo, 0, 0, (int)getSize().getWidth(), (int)getSize().getHeight(),this);
		colorado.dibujar(g);
	}
}
