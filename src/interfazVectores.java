
import javax.swing.*;
import java.awt.*;
import java.awt.Event.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.StringTokenizer;
import java.util.Vector;

public class interfazVectores extends JFrame
{
	Vectores vector;
	JButton b1, b2, b3;
	JTextField ct1;
	JTextField vct[];
	JPanel centro;

	public interfazVectores()
	{

		setSize(900, 250);
		setResizable(false);
		vector = new Vectores(3); // vacio
		vct = new JTextField[vector.capacidad()];
		centro = new JPanel();
		for (int i = 0; i < vct.length; i++)
		{
			vct[i] = new JTextField(3);
			vct[i].setFont(new Font("Arial", Font.BOLD, 20));
			centro.add(vct[i]);
		}
		add(centro, BorderLayout.CENTER);
		JPanel oper = new JPanel(new GridLayout(4, 1));
		b1 = new JButton("Agregar");
		ct1 = new JTextField(5);
		b2 = new JButton("Agregar Varios");
		b3 =new JButton("Borrar");
		ct1.setFont(new Font("Arial", Font.BOLD, 20));
		oper.add(ct1);
		oper.add(b1);
		oper.add(b2);
		oper.add(b3);
		add(oper, BorderLayout.WEST);
		b1.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				String valor = ct1.getText();
				ct1.setText("");
				try
				{
					int val = Integer.parseInt(valor);
					vector.agregar(val);
					actCentro();
				}
				catch (NumberFormatException e)
				{
					JOptionPane.showMessageDialog(null, "Debe digitar un numero");
				}
			}
		});
		b2.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				String cont = ct1.getText();
				ct1.setText(" ");
				StringTokenizer st = new StringTokenizer(cont, ", ");
				int numeros[] = new int[st.countTokens()];
				int ind = 0;
				try
				{
					while (st.hasMoreTokens())
					{
						numeros[ind] = Integer.parseInt(st.nextToken());
						ind++;
					}
					vector.agregar(numeros);
					actCentro();
				}
				catch (NumberFormatException e1)
				{
					JOptionPane.showMessageDialog(null, "Son numericos separados por ',' o por espacios");
				}
			}
		});
		b3.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0)
			{
				String valor = ct1.getText();
				try
				{
					int val=Integer.parseInt(valor);
					vector.eliminar(val);
					actCentro();
				}
				catch (NumberFormatException e2)
				{
					JOptionPane.showMessageDialog(null, "Debe ser numerico");
				}
			}
		});

		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void actCentro()
	{
		centro.removeAll();
		centro.validate();
		vct = new JTextField[vector.NumEle()];
		
		for (int i = 0; i < vct.length; i++)
		{
			vct[i] = new JTextField(vector.vec[i] + " ", 3);
			vct[i].setFont(new Font("Arial", Font.BOLD, 20));
			centro.add(vct[i]);
		}
		centro.validate();
		SwingUtilities.updateComponentTreeUI(centro);
	}

	public static void main(String[] args)
	{
		new interfazVectores();
	}
}
